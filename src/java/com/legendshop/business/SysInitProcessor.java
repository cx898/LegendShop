/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.core.AttributeKeys;
import com.legendshop.core.tag.TableCache;
import com.legendshop.event.processor.BaseProcessor;
import com.legendshop.spi.service.SystemParameterService;

/**
 * 系统初始化事件 eventId: SYS_INIT.
 */
public class SysInitProcessor extends BaseProcessor<ServletContextEvent> {

	/** The system parameter service. */
	private SystemParameterService systemParameterService;

	/** The code tables cache. */
	private TableCache codeTablesCache;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.event.processor.AbstractProcessor#process(java.lang.Object
	 * )
	 */
	@Override
	public void process(ServletContextEvent event) {
		ServletContext servletContext = event.getServletContext();
		// Load System Parameter
		systemParameterService.initSystemParameter();

		codeTablesCache.initCodeTablesCache();

		servletContext.setAttribute("LEGENDSHOP_DOMAIN_NAME", AttributeKeys.LEGENDSHOP_DOMAIN_NAME);

	}

	/**
	 * Sets the system parameter service.
	 * 
	 * @param systemParameterService
	 *            the new system parameter service
	 */
	public void setSystemParameterService(SystemParameterService systemParameterService) {
		this.systemParameterService = systemParameterService;
	}

	/**
	 * Sets the code tables cache.
	 * 
	 * @param codeTablesCache
	 *            the new code tables cache
	 */
	public void setCodeTablesCache(TableCache codeTablesCache) {
		this.codeTablesCache = codeTablesCache;
	}

}
