<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<div class="mc">
		<div id="step">
            <ul>
               <li class="one one1">填写账户名</li>
               <li class="two two1">验证身份</li>
               <li class="three">设置新密码</li>
               <li class="four cur four1">完成</li>
            </ul>
        </div>
		<div class="form" style="margin-bottom: 20px;">
			 <div class="call suc m-return">
                <b style="margin-right: -33px;"></b><span><strong>新密码设置成功!</strong><br>请牢记您新设置的密码。<a href="${applicationScope.SYSTEM_CONFIG.url}">返回首页</a></span>
             </div>
		</div>
		<!--[if !ie]>form end<![endif]-->
		
		<span class="clr"></span>
			
	</div>
	
