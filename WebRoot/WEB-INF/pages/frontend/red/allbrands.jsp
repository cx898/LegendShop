<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<script type=text/javascript>
$(document).ready(function() {
	$(".l1:eq(0)").addClass("curr");
	var url = "${pageContext.request.contextPath}/queryBrands/"+$(".l1:eq(0)").val();
	sendData(url);
	
	$(".l1").bind("click", function() {
  		$(".l1").removeClass("curr");
  		var _this = $(this);
 		 _this.addClass("curr");
			var url = "${pageContext.request.contextPath}/queryBrands/"+_this.val();
			sendData(url);
	});
});

function sendData(url){
    			$.ajax({
				"url":url, 
				type:'post', 
				async : true, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
			 		// alert(textStatus, errorThrown);
				},
				success:function(result){
				   $("#BrandList").html(result);
				}
				});
	}
</script>

<div class="w pagetab "  style="padding-top:20px;">
       <ul>           
          <li><span><a href="<ls:url address='/allsorts'/>">全部商品分类</a></span></li>                  
         <li class="on"><span><a href="<ls:url address='/allbrands'/>">全部品牌</a></span></li>
         <li><span><a href="<ls:url address='/allprods'/>">全部商品</a></span></li>      
       </ul>       
</div>
<!----两栏---->
 <div class="w" style="padding-top:10px;"> 
      <div class="brandbox">
        <div class="mt">
			<h2>推荐品牌</h2>
		</div>
		<div class="mc brandslist">
					<ul class="list-h">
						<c:forEach items="${requestScope.allbrands}" var="brand">
							<li>
								<div>
								<c:choose>
									<c:when test="${brand.brandPic == null}">
										<span class="b-img">
											<a href="#" ><img width="138" height="46" src="../common/red/images/brandicon.jpg" /></a>
										</span>
									</c:when>
									<c:otherwise>
										<span class="b-img">
											<a href="#" ><img width="138" height="46" src="<ls:photo item='${brand.brandPic}'/>" /></a>
										</span>
									</c:otherwise>
								</c:choose>
									<span class="b-name">
										<a href="#" >${brand.brandName}</a>
									</span>
								</div>
							</li>
						</c:forEach>
		</div>
        <div class="clear"></div>
        <div class="i-w" style="background:none; border:0; padding-top:30px;">			
			<ul class="tab" >
				<c:forEach items="${requestScope.sortList}" var="sort" end="10">    
	 					<li class="l1" value="${sort.sortId}">${sort.sortName}</li>
				</c:forEach> 			
			</ul>
        </div>
 	<div align="center"  id="BrandList" name="BrandList"></div>
  </div>
<!----两栏end---->
