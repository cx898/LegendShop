<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<c:forEach  items="${requestScope.floorList}" var="floor"  varStatus="floorIndex">
<div class="w banner1">
	<img src="${pageContext.request.contextPath}/common/red/images/index_adv.jpg" />
</div>
<div class="w">
	<!----右边---->
	<div class="index_right">
		<div class="side2">
		<c:choose>
			<c:when test="${floor.contentType eq 'BD' }"><h3>品牌旗舰店</h3>
				<div class="banddiv">
		          <table border="1">
		           <tr>
		          	<c:forEach items="${floor.floorItems.BD}" var="brand"  varStatus="status">
						<td style="width:86px;height:30px" ><a href="#"><img style="width:86px;height:30px" title="${brand.brandName}" src="<ls:images item='${brand.pic}' scale='2'/>" /></a></td>
						<c:if test="${status.count ==3}"></tr><tr></c:if>
						<c:if test="${status.count ==6}"></tr><tr></c:if>
		          	</c:forEach>
				   </tr>
		          </table>
	         	</div>
			</c:when>
			<c:when test="${floor.contentType eq 'PT' }">
				<h3>商品排行</h3>
				<div class="banddiv" style="padding-top: 5px; padding-bottom: 5px;">
 					<ul style="padding-left: 8px">
        				<c:forEach items="${floor.floorItems.PT}" var="rank"  varStatus="status">
		 					<li class="liranktext" >
		 					<a href="${pageContext.request.contextPath}/views/${rank.referId}"  target="_blank">
		 					<img title="${rank.sortName}" style="width:40px;" src="<ls:images item='${rank.pic}' scale='3'/>" />
		 					    &nbsp;${rank.sortName}</a>
		 					 </li>    
       					</c:forEach>
 					</ul>
 				</div>
			</c:when>
				<c:when test="${floor.contentType eq 'GP' }">
				<h3>今日团购</h3>
				<c:forEach items="${floor.floorItems.GP}" var="group"  varStatus="status">
				<div class="sm_pic">
					<img src="<ls:images item='${group.pic}' scale='3'/>" width="85" height="85" />
				</div>
			<p class="it_info">
				<c:if test="${fn: length(group.brief)>80}">
					${fn: substring(group.brief, 0, 81)}...
				</c:if>
				<c:if test="${fn: length(group.brief)<=80}">
					${group.brief}
				</c:if> <br/> 
				<span class="graytxt" style="text-decoration: line-through;"> 
				 	 原价:<fmt:formatNumber type="currency" value="${group.price}" pattern="${CURRENCY_PATTERN}" />
				</span>
			</p>
			<div class="itprice">
				<a href="${pageContext.request.contextPath}/group/view/${group.referId}" target="_blank">
						团购价:<span><fmt:formatNumber type="currency"  value="${group.cash}" pattern="${CURRENCY_PATTERN}" /></span>
				</a>
			</div>
			</c:forEach>
		    </c:when>
			</c:choose>
		</div>

		<div style="margin-top: 10px;">
			<img src="${pageContext.request.contextPath}/common/red/images/radv1.jpg" />
		</div>
		<div style="margin-top: 10px;">
			<img src="${pageContext.request.contextPath}/common/red/images/radv2.jpg" />
		</div>


	</div>
	<!----右边end---->

	<!----左边---->
	<div class="index_left">
		<div class='indexlp 		
		<c:choose>
			<c:when test="${floor.cssStyle eq 1 }">blueback</c:when>
			<c:when test="${floor.cssStyle eq 2 }">greenback</c:when>
			<c:when test="${floor.cssStyle eq 3 }">redback</c:when>
		</c:choose>'>
			<h3>${floorIndex.count}F ${floor.name }</h3>
			<ul>
			<c:forEach items="${floor.subFloors}" var="subFloor"  varStatus="index">
					<li  id="${subFloor.sfId }" class='l1 <c:if test="${index.index eq 0 }">focus</c:if>'><a >${subFloor.name }</a></li> 
			</c:forEach>
			</ul>
		</div>
		<div class="indexlpdiv">
			<div class="lpleft smback"> <!--  -->
			            <c:forEach items="${floor.floorItems.ST}" var="floorItem" varStatus="index">
				        <ul style="clear: both;">
				         	 <h6>
				         	 <c:choose>
				         	 		<c:when test="${floorItem.sortLevel eq 1}"> <a href="${pageContext.request.contextPath}/sort/${floorItem.referId}"  target="_blank"></c:when>
				         	 		<c:otherwise><!-- todo --></c:otherwise>
				         	 </c:choose>
				         	 ${floorItem.sortName}</a>
				         	</h6>
				              <c:forEach items="${floorItem.floorSubItemSet}" var="floorSubItem">
				              		<li><a href="#">${floorSubItem.nsortName}</a></li>
				              </c:forEach>
				        </ul>
				        	</c:forEach>
			</div>
			<div class="lptable">
				<!-- product table start -->
				<c:forEach items="${floor.subFloors}" var="subFloor"  varStatus="index">
				<c:choose>
						<c:when test="${index.index eq 0 }"><table id="productItemTable${subFloor.sfId}" style="margin-top: 5px; margin-bottom: 5px; display: block"></c:when>
						<c:otherwise><table id="productItemTable${subFloor.sfId}" style="margin-top: 5px; margin-bottom: 5px; display: none"></c:otherwise>
				</c:choose>
					<tr>
						<c:forEach items="${subFloor.subFloorItems}" var="subFloorItem"  varStatus="status">
						<td>
						<div class="ppicdiv" style="height:100px;"><a href="${pageContext.request.contextPath}/views/${subFloorItem.productId}" target="_blank"><img src="<ls:images item='${subFloorItem.productPic}' scale='2' />" /></a></div>
						<p>
							<c:if test="${fn: length(subFloorItem.productName)>18}">
								${fn: substring(subFloorItem.productName, 0, 19)}...
							</c:if>
							<c:if test="${fn: length(subFloorItem.productName)<=18}">
								${subFloorItem.productName}
							</c:if>
						<br/>商城价：<span>${subFloorItem.productPrice}</span></p>
						</td>
						<c:if test="${status.count ==4}"></tr><tr></c:if>
						</c:forEach>
					</tr>
			</table>
			</c:forEach>
				<!-- product table end -->
			</div>
			
			<div class="clear"></div>
		</div>




	</div>
	<!----左边end---->

	<div class="clear"></div>
</div>
</c:forEach>
    <script type="text/javascript">
 $(document).ready(function() {
    	$(".l1").bind("mouseover", function() {
    	     var _this = $(this);
    	     var id = _this.attr("id");
    	     _this.parent().children().removeClass("focus");
     		 _this.addClass("focus");
     		 var productItemTable = $("#productItemTable" + id);
     		 productItemTable.parent().children().hide();
     		 productItemTable.show();
    	});
    });
    </script>