<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<!--nav-->

 <div class="nav">
   <div class="wrap">
   <p class="dengl">
   <span class="yongh">验证失败<input type="hidden" id="redirectIndicator" name="redirectIndicator" value="${pageContext.request.contextPath}/failPage"/></span>
   </p>
   </div> 
 </div>
<!--nav end-->

<div class="w"> 
    <div class="login_left wrap">
      <div class="news_wrap">
         <div class="news_bor" id="failpage">
			    <div class="mc">
  					<i class="reg-error"></i>
					
        			<div class="reg-tips-info" style="color: #CC0000;margin-top: 20px;font-weight: bold;">抱歉，验证链接已失效！</div>
       				<div class="reg-nickname-tips"> 您可以前往<a href="${DOMAIN_NAME}/p/usercenter?tab=security" style="color: #005EA7;">安全中心</a>继续验证。</div>
   				 </div> 
        </div>      
      </div>                                  
    </div>
    <!----左边end---->
    
   <div class="clear"></div>
</div>  