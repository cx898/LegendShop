<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@page import="com.legendshop.core.helper.PropertiesUtil"%>
<%@page import="com.legendshop.core.helper.ThreadLocalContext"%>
 
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<lb:shopDetail var="shopDetail" />
<link href="${pageContext.request.contextPath}/common/default/css/header.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/top.js"></script>
<!--顶部menu-->
<div id="shortcut">
	<div class="w">
		<ul class="fl lh">
		 <ls:plugin pluginId="advanceSearch">
		  <li  class="fore1"><a href="${pageContext.request.contextPath}/all" target="_blank"><b><ls:i18n key="search.total.index" /></b></a></li>	
		  </ls:plugin>
		  <c:if test="${'C2C' == applicationScope.BUSINESS_MODE}">
				<li><a href=" ${pageContext.request.contextPath}/home"><ls:i18n key="shop.total.index" /></a></li>
		   </c:if>
		  <li>
				<ls:i18n key="nows.location" />
				<c:if test="${shopDetail.province != null}">
				  		 ${shopDetail.province}/${shopDetail.city}/${shopDetail.area}/
				  </c:if>
				<b><a href="${pageContext.request.contextPath}/shopcontact"><lb:currentShop /></a>
				</b>
		  </li>	
			<c:if test="${'C2C' == applicationScope.BUSINESS_MODE}">
				<li>
					<a href="${pageContext.request.contextPath}/reg?openshop=1" target="_blank"><ls:i18n
							key="register.shop" />
					</a>
				</li>
			</c:if>
			<li class="favorite">
				<a href='#'
					onclick="javascript:bookmark('<ls:i18n key="welcome.to.legendshop"/><%=PropertiesUtil.getDefaultShopName()%> - LegendShop购物商城','<ls:domain shopName="<%=PropertiesUtil.getDefaultShopName()%>" />');">
					<ls:i18n key="shop.favorite" />
				</a>
			</li>
		</ul>
		<ul class="fr lh">
		<span id="user_info">
			 <c:choose>
			   <c:when test="${sessionScope.SPRING_SECURITY_LAST_USERNAME != null}">
						<li id="loginbar"  class="ld">
						<font color="#FF3300" style="font-weight: bold;">${sessionScope.SPRING_SECURITY_LAST_USERNAME}</font>
						<span>
						<a href="<ls:url address='/p/usercenter'/>")>[<ls:i18n key="myaccount"/>]</a> 
						<a href="${pageContext.request.contextPath}/p/logout" target="_parent">[<ls:i18n key="logout"/>]</a>
						</span></li>
			   			<ls:auth ifAnyGranted="FE_BACKEND">
							<li ><a href="<ls:url address='/admin/index'/>"><b><fmt:message key="system.management"/></b></a></li>
						</ls:auth>
						<li><a href="<ls:url address='/p/usercenter?tab=myorder'/>">我的订单</a></li>
			   </c:when>
			   <c:otherwise>
						<li id="loginbar"  class="ld">
						<span><a href="<ls:url address='/login'/>">[<ls:i18n key="login"/>]</a> 
						<a class="link-regist" href="<ls:url address='/reg'/>">[<ls:i18n key="regFree"/>]</a></span></li>
			   </c:otherwise>
			</c:choose>
		 </span>
	    <li><a href="<ls:url address='/allnews'/>"><ls:i18n key="newsCenter"/></a></li>
		<li class="menu">
		  <div>
		  <dt class="ld">
		  		<a href="${applicationScope.LEGENDSHOP_DOMAIN_NAME}" target="_blank"><ls:i18n key="club.navigation" /></a>
		  </dt>
		  </div>
		  <div style="position: absolute;right:0;">
		  <dl>
		  		  		<c:forEach items="${requestScope.navigationList}" var="navigation" end="4">
									   		<dd>
												<div>
													<b>${navigation.name}</b><br/>
												<div>
												<div>
													<c:forEach items="${navigation.subItems}" var="item">
														<a href="${item.link}" target="_blank">${item.name}</a>
													</c:forEach>
													</div>
												</dd>
												</dt>
											</dd>
						</c:forEach>
			 </dl>
		</div>
	</li>
	  </ul>
		<span class="clr"></span>
	</div>

</div>
<script type=text/javascript>
$(document).ready(function() {
	$(".menu").mouseover(function(){
		var _this = $(this);
		 _this.addClass("on");
	});
	$(".menu").mouseout(function(){
		var _this = $(this);
		 _this.removeClass("on");
	});
});

//login pop window call back function
	function loginSuccess(){
			$.ajax({
				url:"${pageContext.request.contextPath}/topuserinfo", 
				type:'post', 
				async : false, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
				},
				success:function(data){   
				   $('#user_info').html(data);
				}   
				});  
	}
</script>
<!--顶部menu end-->