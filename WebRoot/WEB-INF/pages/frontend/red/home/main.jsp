<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<!----advtisement---->
<div class="w" style="margin-top: 10px; height: 485px;">
	<div class="indexgg indexggCC">
		<div class="ggtit">
			<div class="ggmore">
				<a href="${pageContext.request.contextPath}/allnews"><img src="<ls:templateResource item='/common/red/images/gg_more.jpg'/>" width="56" height="39" /></a>
			</div>
			<h3>商城公告</h3>
		</div>
		<ul class="lglist">
			<c:forEach items="${pubList}" var="pub" begin="0" end="5">
				<li><a href="${pub.msg}" title="${pub.title}">${pub.title}</a></li>
			</c:forEach>
		</ul>


		<div class="setshop">
			<a href="${pageContext.request.contextPath}/reg?openshop=1"><img src="${pageContext.request.contextPath}/common/red/images/setshop.jpg" width="324"
				height="57" /></a>
		</div>
	</div>

	<div class="indexflash">
		<img src="${pageContext.request.contextPath}/common/red/images/advsample.jpg" width="650" height="335" />
		
		<c:forEach items="${indexJpgList}" var="slideAdv" begin="2" end="0">
			<a href="${slideAdv.link}" title="${slideAdv.title}">
			<img src="<ls:photo item='${slideAdv.img}'/>" alt="${slideAdv.alt}" width="650" height="335"> </a>
		</c:forEach>
		<div class="fla_num">
			<a href="#">1</a><a href="#" class="focus">2</a><a href="#">3</a><a
				href="#">4</a><a href="#">5</a><a href="#">6</a>
		</div>
	</div>
	<div class="indexhot">
		<img src="${pageContext.request.contextPath}/common/red/images/222_150_MdQlQS.jpg" width="222" height="150" />
		<img src="${pageContext.request.contextPath}/common/red/images/222_150_MdQlQS.jpg" width="222" height="150" /> 
		<img src="${pageContext.request.contextPath}/common/red/images/222_150_MdQlQS.jpg" width="222" height="150" />
		<img src="${pageContext.request.contextPath}/common/red/images/222_150_MdQlQS.jpg" width="222" height="150" />
		<img src="${pageContext.request.contextPath}/common/red/images/222_150_MdQlQS.jpg" width="222" height="150" />
	</div>
	<div class="clear"></div>
</div>


<!----up两栏---->
<div class="w">
	<!----右边---->
	<div class="index_right">
		<div class="side2">
			<h3>热销产品</h3>

			<div class="CChot">
				<div class="CCpic">
					<img
						src="<ls:templateResource item='/common/red/images/cloth.jpg'/>"
						width="150" height="150" />
				</div>
				<p class="CC-name">新款长袖针织衫</p>
				<p class="CC-price">
					￥<span>148.00</span>
				</p>
				<p class="CC-num mailfree">
					<input name="" type="image" src="${pageContext.request.contextPath}/common/red/images/mailfree.jpg" />月销<span>279</span>
				</p>
			</div>

			<div class="CChot">
				<div class="CCpic">
					<img src="${pageContext.request.contextPath}/common/red/images/cloth.jpg" width="150" height="150" />
				</div>
				<p class="CC-name">新款长袖针织衫</p>
				<p class="CC-price">
					￥<span>148.00</span>
				</p>
				<p class="CC-num mailfree">
					<input name="" type="image" src="${pageContext.request.contextPath}/common/red/images/mailfree.jpg" />月销<span>279</span>
				</p>
			</div>

			<div class="ccline"></div>

			<div class="CChot">
				<div class="CCpic">
					<img src="${pageContext.request.contextPath}/common/red/images/cloth.jpg" width="150" height="150" />
				</div>
				<p class="CC-name">新款长袖针织衫</p>
				<p class="CC-price">
					￥<span>148.00</span>
				</p>
				<p class="CC-num mailfree">
					<input name="" type="image" src="${pageContext.request.contextPath}/common/red/images/mailfree.jpg" />月销<span>279</span>
				</p>
			</div>

			<div class="CChot">
				<div class="CCpic">
					<img src="${pageContext.request.contextPath}/common/red/images/cloth.jpg" width="150" height="150" />
				</div>
				<p class="CC-name">新款长袖针织衫</p>
				<p class="CC-price">
					￥<span>148.00</span>
				</p>
				<p class="CC-num">
					<input name="" type="image" src="${pageContext.request.contextPath}/common/red/images/mailfree.jpg" />月销<span>279</span>
				</p>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!----右边end---->

	<!----左边---->
	<div class="index_left">
		<div class="CCbox">
			<h2 class="CCtit">
				<span><a href="#">更多品牌 &gt;&gt;</a></span>品牌导航
			</h2>
			<div class="banddiv" style="padding: 0;">
				<table border="1">
					<tr>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
					</tr>
					<tr>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
						<td><a href="#"><img src="${pageContext.request.contextPath}/common/red/images/bandl.jpg" /></a></td>
					</tr>

				</table>
			</div>
		</div>

		<div class="CCbox" style="margin-top: 10px;">
			<h2 class="CCtit">
				<span><a href="#">更多商城 &gt;&gt;</a></span>明星商城
			</h2>
			<div class="startable">
				<table>
					<tr>
						<td><div class="starpic">
								<img src="${pageContext.request.contextPath}/common/red/images/star.jpg" width="125" height="125" />
							</div>
							<p class="storename">
								<a href="#">优质店铺珍藏馆</a>
							</p>
							<p class="starname">
								<a href="#">@lw050123</a>
							</p></td>
						<td><div class="starpic">
								<img src="${pageContext.request.contextPath}/common/red/images/star.jpg" width="125" height="125" />
							</div>
							<p class="storename">
								<a href="#">优质店铺珍藏馆</a>
							</p>
							<p class="starname">
								<a href="#">@lw050123</a>
							</p></td>
						<td><div class="starpic">
								<img src="${pageContext.request.contextPath}/common/red/images/star.jpg" width="125" height="125" />
							</div>
							<p class="storename">
								<a href="#">优质店铺珍藏馆</a>
							</p>
							<p class="starname">
								<a href="#">@lw050123</a>
							</p></td>
						<td><div class="starpic">
								<img src="${pageContext.request.contextPath}/common/red/images/star.jpg" width="125" height="125" />
							</div>
							<p class="storename">
								<a href="#">优质店铺珍藏馆</a>
							</p>
							<p class="starname">
								<a href="#">@lw050123</a>
							</p></td>
						<td><div class="starpic">
								<img src="${pageContext.request.contextPath}/common/red/images/star.jpg" width="125" height="125" />
							</div>
							<p class="storename">
								<a href="#">优质店铺珍藏馆</a>
							</p>
							<p class="starname">
								<a href="#">@lw050123</a>
							</p></td>
						<td><div class="starpic">
								<img src="${pageContext.request.contextPath}/common/red/images/star.jpg" width="125" height="125" />
							</div>
							<p class="storename">
								<a href="#">优质店铺珍藏馆</a>
							</p>
							<p class="starname">
								<a href="#">@lw050123</a>
							</p></td>

					</tr>
					<tr>
						<td><div class="starpic">
								<img src="${pageContext.request.contextPath}/common/red/images/star.jpg" width="125" height="125" />
							</div>
							<p class="storename">
								<a href="#">优质店铺珍藏馆</a>
							</p>
							<p class="starname">
								<a href="#">@lw050123</a>
							</p></td>
						<td><div class="starpic">
								<img src="${pageContext.request.contextPath}/common/red/images/star.jpg" width="125" height="125" />
							</div>
							<p class="storename">
								<a href="#">优质店铺珍藏馆</a>
							</p>
							<p class="starname">
								<a href="#">@lw050123</a>
							</p></td>
						<td><div class="starpic">
								<img src="${pageContext.request.contextPath}/common/red/images/star.jpg" width="125" height="125" />
							</div>
							<p class="storename">
								<a href="#">优质店铺珍藏馆</a>
							</p>
							<p class="starname">
								<a href="#">@lw050123</a>
							</p></td>
						<td><div class="starpic">
								<img src="${pageContext.request.contextPath}/common/red/images/star.jpg" width="125" height="125" />
							</div>
							<p class="storename">
								<a href="#">优质店铺珍藏馆</a>
							</p>
							<p class="starname">
								<a href="#">@lw050123</a>
							</p></td>
						<td><div class="starpic">
								<img src="${pageContext.request.contextPath}/common/red/images/star.jpg" width="125" height="125" />
							</div>
							<p class="storename">
								<a href="#">优质店铺珍藏馆</a>
							</p>
							<p class="starname">
								<a href="#">@lw050123</a>
							</p></td>
						<td><div class="starpic">
								<img src="${pageContext.request.contextPath}/common/red/images/star.jpg" width="125" height="125" />
							</div>
							<p class="storename">
								<a href="#">优质店铺珍藏馆</a>
							</p>
							<p class="starname">
								<a href="#">@lw050123</a>
							</p></td>

					</tr>

				</table>

			</div>

		</div>
	</div>
	<!----左边end---->
	<div class="clear"></div>
</div>
<!----up两栏end---->

<!----楼层开始---->
 <div id="floorList" name="floorList"></div>
<!----楼层结束---->

<!----down两栏---->
<div class="w">
	<!----右边---->
	<div class="index_right">
		<div class="side2">
			<h3>时尚资讯</h3>
			<ul class="lista font12 blue_a listimg1" style="margin-left: 10px;">
				<li><a href="#">品牌女包季末清仓69元起</a></li>
				<li><a href="#">我比美食更有诱惑力！---韩国明进工坊</a></li>
				<li><a href="#">樱春季 忙护理 资生堂全场满196赠！</a></li>
				<li><a href="#">IDF买就送精美丝巾</a></li>
				<li><a href="#">三月迎春，最爱就“饰”你！</a></li>
				<li><a href="#">三月迎春，最爱就“饰”你！</a></li>
				<li><a href="#">三月迎春，最爱就“饰”你！</a></li>
			</ul>
		</div>
	</div>
	<!----右边end---->

	<!----左边---->
	<div class="index_left">
		<div class="hotevent">
			<div class="eventbox">
				<h3>热门晒单</h3>
				<div class="eventlist">
					<ul>
						<li style="height: 60px; display: list-item;">
							<div class="p-img">
								<a href="#"><img width="50" height="50"
									src="http://img10.360buyimg.com/N5/3015/b0724ba8-7f85-4f3a-bb61-3c2bc7a8e341.jpg"><b
									class="ci cix1"></b></a>
							</div>
							<div class="p-name">
								<a href="#">质量可靠，系统稳定，值得拥有</a>
							</div>
							<div class="p-detail">
								<a href="#">dell+百度确实是个好的组，便于上手。大屏耗电是软肋，节约情况下2至3天待机应该无悬念，具体见我的评价。</a>
							</div>
						</li>
						<li style="height: 60px; display: list-item;">
							<div class="p-img">
								<a href="#"><img width="50" height="50"
									src="http://img10.360buyimg.com/N5/3015/b0724ba8-7f85-4f3a-bb61-3c2bc7a8e341.jpg"><b
									class="ci cix1"></b></a>
							</div>
							<div class="p-name">
								<a href="#">质量可靠，系统稳定，值得拥有</a>
							</div>
							<div class="p-detail">
								<a href="#">dell+百度确实是个好的组，便于上手。大屏耗电是软肋，节约情况下2至3天待机应该无悬念，具体见我的评价。</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="eventbox">
				<h3>热门活动</h3>
				<div class="eventlist">
					<ul style="border: 0;">
						<li style="height: 60px; display: list-item;">
							<div class="p-img">
								<a href="#"><img width="50" height="50"
									src="http://img10.360buyimg.com/N5/3015/b0724ba8-7f85-4f3a-bb61-3c2bc7a8e341.jpg"><b
									class="ci cix2"></b></a>
							</div>
							<div class="p-name">
								<a href="#">质量可靠，系统稳定，值得拥有</a>
							</div>
							<div class="p-detail">
								<a href="#">dell+百度确实是个好的组，便于上手。大屏耗电是软肋，节约情况下2至3天待机应该无悬念，具体见我的评价。</a>
							</div>
						</li>
						<li style="height: 60px; display: list-item;">
							<div class="p-img">
								<a href="#"><img width="50" height="50"
									src="http://img10.360buyimg.com/N5/3015/b0724ba8-7f85-4f3a-bb61-3c2bc7a8e341.jpg"><b
									class="ci cix2"></b></a>
							</div>
							<div class="p-name">
								<a href="#">质量可靠，系统稳定，值得拥有</a>
							</div>
							<div class="p-detail">
								<a href="#">dell+百度确实是个好的组，便于上手。大屏耗电是软肋，节约情况下2至3天待机应该无悬念，具体见我的评价。</a>
							</div>
						</li>
					</ul>
				</div>
			</div>

			<div class="clear"></div>
		</div>

	</div>
	<!----左边end---->
	<div class="clear"></div>
</div>
<script type="text/javascript">
<!--
	    function loadFloor(){
    			$.ajax({
				url:"${pageContext.request.contextPath}/loadFloor", 
				type:'post', 
				async : true, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
			 		// alert(textStatus, errorThrown);
				},
				success:function(result){
				   $("#floorList").html(result);
				}
				});
	}

	$(document).ready(function() {
		//装载楼层
		loadFloor();
	});
	
//-->
</script>
<!----down两栏end---->
